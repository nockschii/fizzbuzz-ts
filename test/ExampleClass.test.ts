import ExampleClass from "../src/ExampleClass";
import {describe, expect, it} from "@jest/globals";

describe("ExampleTypescript", () => {
    it("should return Hello", () => {
        const exampleClass = new ExampleClass();

        const result = exampleClass.greet();

        expect(result).toBe("Hello");
    });
});
